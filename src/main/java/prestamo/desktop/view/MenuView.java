package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MenuView extends JFrame {

	private JPanel contentPane;
	private JMenuItem mntmSolicitar;

	/**
	 * Create the frame.
	 */
	public MenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPrestamo = new JMenu("Prestamo");
		menuBar.add(mnPrestamo);
		
		mntmSolicitar = new JMenuItem("Solicitar");
		mnPrestamo.add(mntmSolicitar);
		
		JMenuItem mntmConsultar = new JMenuItem("Consultar");
		mnPrestamo.add(mntmConsultar);
		
		JMenu mnPagos = new JMenu("Pagos");
		menuBar.add(mnPagos);
		
		JMenuItem mntmVisaulizar = new JMenuItem("Visaulizar");
		mnPagos.add(mntmVisaulizar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	public JMenuItem getMntmSolicitar() {
		return mntmSolicitar;
	}

	public void setMntmSolicitar(JMenuItem mntmSolicitar) {
		this.mntmSolicitar = mntmSolicitar;
	}
	
	

}
