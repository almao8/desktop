package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.view.MenuView;
import prestamo.desktop.view.prestamoView;

public class MenuController implements ActionListener{
	MenuView view;
	
	public MenuController(MenuView view) {
		this.view = view;
		view.getMntmSolicitar().addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==view.getMntmSolicitar()) {
			prestamoView pv = new prestamoView();
			view.setVisible(false);
			pv.setVisible(true);
		}
	}

}
