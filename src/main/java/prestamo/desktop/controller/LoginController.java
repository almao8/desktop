package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import prestamo.desktop.model.UserModel;
import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.MenuView;

public class LoginController implements ActionListener{
	
	LoginView view;
	
	public LoginController(LoginView view){
		this.view = view;
		this.view.getBtnIniciarSesion().addActionListener(this);
		
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == view.getBtnIniciarSesion()) {
			String user = view.getUserField().getText();
			String pass = view.getPasswordField().getText();
			
			UserModel um = new UserModel();
			if(um.autenticate(user, pass)) {
				view.setVisible(false);
				MenuView mv = new MenuView();
				MenuController mc = new MenuController(mv);
				mv.setVisible(true);
			}else {
				JOptionPane.showMessageDialog(null,"error al validar usuario");
			}
		}
	}

}
