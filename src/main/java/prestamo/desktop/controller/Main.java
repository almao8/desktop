package prestamo.desktop.controller;

import prestamo.desktop.view.LoginView;

public class Main {
	
	public static void main(String[] args) {
		LoginView lv =  new LoginView();
		
		LoginController lc =  new LoginController(lv);
		
		lv.setVisible(true);
		
	}

}
